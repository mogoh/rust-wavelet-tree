/// A Binary Tree
#[derive(Debug)]
pub struct BinaryTree<T> {
	item: T,
	left: Option<Box<BinaryTree<T>>>,
	right: Option<Box<BinaryTree<T>>>,
}

impl<T> BinaryTree<T> {
	/// Constructor
	///
	/// # Arguments
	///
	/// * `root` - The root of the Binary Tree that is to be created
	pub fn new(root: T) -> BinaryTree<T> {
		BinaryTree {
			item: root,
			left: None,
			right: None,
		}
	}

	/// Overwrites the value that is hold in the current Binary Tree
	///
	/// # Arguments
	///
	/// * `val` - The overwriting value
	pub fn insert(&mut self, val: T) {
		self.item = val;
	}

	/// Creates a left sub-Tree
	///
	/// # Arguments
	///
	/// * `val` - root of the new sub-Tree
	pub fn set_left(&mut self, left_tree: BinaryTree<T>) {
		if self.left.is_none() {
			self.left = Some(Box::new(left_tree));
		}
	}

	/// Creates a right sub-Tree
	///
	/// # Arguments
	///
	/// * `val` - root of the new sub-Tree
	pub fn set_right(&mut self, right_tree: BinaryTree<T>) {
		if self.right.is_none() {
			self.right = Some(Box::new(right_tree));
		}
	}

	/// Returns the currently hold value
	pub fn get_item(&self) -> &T {
		&self.item
	}

	/// Returns the left sub-Tree of the current Tree
	pub fn get_left(&self) -> Option<&BinaryTree<T>> {
		self.left.as_ref().map(|child| child.as_ref())
	}

	/// Returns the right sub-Tree of the current Tree
	pub fn get_right(&self) -> Option<&BinaryTree<T>> {
		self.right.as_ref().map(|child| child.as_ref())
	}

	pub fn is_leaf(&self) -> bool {
		self.left.is_none() && self.right.is_none()
	}

	pub fn has_left_child(&self) -> bool {
		!self.left.is_none()
	}

	pub fn has_right_child(&self) -> bool {
		!self.right.is_none()
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn get_simple_root() {
		let bin_tree = BinaryTree::new(3);
		assert_eq!(3, *(bin_tree.get_item()));
	}

	#[test]
	fn is_leaf() {
		let bin_tree = BinaryTree::new(3);
		assert!(!bin_tree.has_left_child());
		assert!(!bin_tree.has_right_child());
		assert!(bin_tree.is_leaf());
	}

	#[test]
	fn get_simple_left() {
		let mut bin_tree = BinaryTree::new(3);
		bin_tree.set_left(BinaryTree::new(4));
		assert_eq!(4, *bin_tree.get_left().unwrap().get_item());
	}
}
