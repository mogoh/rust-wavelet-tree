// Disable warnings for the whole crate
#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_imports)]
#![allow(unreachable_code)]

// Imports for quickcheck
#[cfg(test)]
#[macro_use(quickcheck)]
extern crate quickcheck_macros;
#[cfg(test)]
extern crate quickcheck;

mod binary_tree;

mod graph;
mod wavelet_tree;
use std::str;
use std::str::from_utf8;
use wavelet_tree::WaveletTreeP;
use wavelet_tree::WaveletTreeNP;
use std::ops::Range;
use graph::Graph;

fn main() {
	
}
