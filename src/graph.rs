use crate::wavelet_tree::WaveletTreeP;

use bio::data_structures::rank_select::RankSelect;
use bv::bit_vec;
use bv::BitSlice;
use bv::BitVec;

/// The Graph exists of:
///
/// * `adjacency_list`: the adjacency list
/// * `rank_select`: a bit vector for saving points of separation
///
/// For now the graph only saves isize numbers. This is for simplicity reasons and
/// has to change later.
pub struct Graph {
    adjacency_list: Option<WaveletTreeP<u64>>,
    seperation_vector: RankSelect,
}

impl Graph {
    /// We do not support an empty graph.
    pub fn new() -> Self {
        Graph {
            adjacency_list: None,
            seperation_vector: RankSelect::new(bit_vec![true], 1),
        }
    }

    /// Add a node to the graph.
    ///
    /// * Add a one to the bit vector.
    /// * The adjacen list stays unchanged.
    pub fn add_node(&mut self) {
        let bv: &BitVec<u8> = self.seperation_vector.bits();
        let mut new_bv: BitVec<u8> = bv.clone();
        new_bv.push(true);
        // todo: test if typecast gone wrong.
        let len = new_bv.len() as usize;
        self.seperation_vector = RankSelect::new(new_bv, len);
    }

    /// Test, if a graph contains a node
    /// We do this by searching through the `node_list`.
    pub fn has_node(&self, node: u64) -> bool {
        let sv_len: u64 = self.seperation_vector.bits().len();
        //let len = self.seperation_vector.rank_1(sv_len);
        let len = match self.seperation_vector.rank_1(sv_len) {
            None => panic!("seperation_vector shorter than it should be."),
            Some(len) => len,
        };
        return len == node;
    }

    pub fn delete_node(&self, node: u64) {
        unimplemented!();
    }

    /// Adds an edge to the graph.
    ///
    /// * Test, if the from- and the to-node are in the graph
    /// * Get the node index
    /// * Add a zero to `rank_select`
    /// * Add the node to the adjacency_list
    pub fn set_edge(&mut self, from: u64, to: u64) {
        if !self.has_node(from) {
            // TODO:
            // from-node non existent
        } else if !self.has_node(to) {
            // TODO:
            // to-node non existent
        } else if self.has_edge(from, to) {
            // Edge already existent. Nothing to do.
        } else {
            //let bv: &BitVec<u8> = self.seperation_vector.bits();
            if self.adjacency_list.is_none() {
                // add a number to the adjaccency list
                self.adjacency_list = WaveletTreeP::new(&[to]);
            } else {
                // add a number to adjacency list.
                let (adjacency_list_start, _) = match self.get_adjacency_list(from) {
                    None => panic!("adjacency_list to short, when it should not."),
                    Some(tupel) => tupel,
                };
                let adjacency_list_start_usize = adjacency_list_start as usize;
                self.adjacency_list
                    .as_mut()
                    .unwrap()
                    .insert(adjacency_list_start_usize, to);
            }
            // add a zero to the seperator
            let splitpoint: u64 = match self.seperation_vector.select_1(from) {
                None => panic!("seperation_vector to short, when it should not."),
                Some(seperation_vector) => seperation_vector,
            };
            // This does not work:
            //let bits = self.seperation_vector.bits();
            //let bits_slice: BitSlice<'_, u8> = bits.as_slice();
            //bits_slice.bit_slice(1..splitpoint);
            // This is not nice:
            let seperation_vector_len: u64 = self.seperation_vector.bits().len();
            let mut i = 0;
            let mut bit_vec: BitVec<u8> = BitVec::new();
            while i <= splitpoint {
                bit_vec.push(self.seperation_vector.get(i));
                i += 1;
            }
            bit_vec.push(false);
            while i < seperation_vector_len {
                bit_vec.push(self.seperation_vector.get(i));
                i += 1;
            }
            let seperation_vector_len_usize = seperation_vector_len as usize;

            self.seperation_vector = RankSelect::new(bit_vec, seperation_vector_len_usize)
        }
    }

    pub fn has_edge(&self, from: u64, to: u64) -> bool {
        if !self.has_node(to) {
            // to-node non existent
            return false;
        } else {
            let adjacency_list: &WaveletTreeP<u64> = match &self.adjacency_list {
                None => return false,
                Some(adjacency_list) => adjacency_list,
            };
            let (adjacency_list_start, adjacency_list_end): (u64, u64) =
                match self.get_adjacency_list(from) {
                    None => return false,
                    Some(tuple) => tuple,
                };
            let mut i: u64 = adjacency_list_start;
            while i < adjacency_list_end {
                if adjacency_list.access(i) == to {
                    return true;
                }
                i += 1;
            }
            return false;
        }
    }

    pub fn delete_edge(&self, from: u64, to: u64) {
        unimplemented!();
    }

    pub fn get_succ(&self, node: u64) -> Option<Vec<u64>> {
        match &self.adjacency_list {
            None => None,
            Some(adjacency_list) => {
                let mut result: Vec<u64> = Vec::new();
                let (adjacency_list_start, adjacency_list_end): (u64, u64) = match self.get_adjacency_list(node)
                {
                    None => panic!("adjacency_list to short, when it should not."),
                    Some(tupel) => tupel,
                };
                let mut i: u64 = adjacency_list_start;
                while i < adjacency_list_end {
                    result.push(adjacency_list.access(i));
                    i += 1;
                }
                return Some(result);
            }
        }
    }

    pub fn get_pred(&self, node: u64) -> Option<Vec<u64>> {
        match &self.adjacency_list {
            None => None,
            Some(adjacency_list) => {
                let mut result: Vec<u64> = Vec::new();
                let adjacency_list_len: u64 = adjacency_list.len();
                let pred_number: u64 = adjacency_list.rank(adjacency_list_len, node-1);
                let mut i = 1;
                while i <= pred_number {
                    // Get the index in the adjacency_list
                    let index: u64 = adjacency_list.select(i, node).unwrap();
                    // how many 0 untill index?
                    let rank: u64 = self.seperation_vector.rank_0(index).unwrap();
                    // how many 1 untill rank?
                    let result_rank: u64 = self.seperation_vector.rank_1(rank).unwrap();
                    result.push(result_rank);
                    i+=1;
                }
                return Some(result);
            }
        }
    }

    fn get_adjacency_list(&self, node: u64) -> Option<(u64, u64)> {
        if !self.has_node(node) {
            // from-node non existent
            None
        } else {
            let seperation_vector_len: u64 = self.seperation_vector.bits().len();
            // seperation_vector_1: the index of the 1 in the seperation vector
            let seperation_vector_1: u64 = self.seperation_vector.select_1(node).unwrap();
            // seperation_vector_0: number of 0 untill starting position - index of adjacency_list start index
            let adjacency_list_start: u64 = (seperation_vector_1 - node) + 1;
            // is there an edge at all?
            if seperation_vector_len < adjacency_list_start {
                // adjacency_list list is to small
                None
            } else if self.seperation_vector.get(adjacency_list_start) == true {
                // there is no edge
                None
            } else {
                // how long is the adjacency_list?
                let seperation_vector_1_1: u64 = self.seperation_vector.select_1(node + 1).unwrap();
                let adjacency_list_end: u64 = seperation_vector_1_1 - node;
                Some((adjacency_list_start, adjacency_list_end))
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn dummy() {
        let rank_select = RankSelect::new(bit_vec![true, false], 1);
        println!("{}", rank_select.select_1(1).unwrap());
        println!("{}", rank_select.select_0(1).unwrap());
        println!("{}", rank_select.rank_1(1).unwrap());
        println!("{}", rank_select.rank_0(1).unwrap());
        rank_select.get(2);
    }

    #[test]
    fn has_node_test() {
        let g = Graph::new();
        assert!(g.has_node(0));
    }

    #[test]
    fn add_node() {
        let mut g = Graph::new();
        g.add_node();
        assert!(g.has_node(3));
        assert!(g.has_node(2));
    }
}
