use crate::binary_tree::BinaryTree;

use bio::data_structures::rank_select::RankSelect;
use bv;
use bv::BitsMut;
use num_traits::cast::{cast, NumCast};
#[cfg(test)]
use rand::seq::SliceRandom;
#[cfg(test)]
use rand::Rng;
use std::fmt;
use std::fmt::Debug;
use std::ops::Add;
use std::ops::Div;
use std::ops::Range;
use std::ops::Sub;
use std::str;

type BitVec = bv::BitVec<u8>;

// Wavelet Tree using Pointers
pub struct WaveletTreeP<T> {
	tree: BinaryTree<(RankSelect, T, T)>,
}

impl<
		T: Ord
			+ Copy
			+ Add<Output = T>
			+ Div<Output = T>
			+ Sub<Output = T>
			+ PartialEq
			+ NumCast
			+ Debug,
	> WaveletTreeP<T>
{
	///Creates a Wavelet Tree from an Iterator
	/// Example:
	/// ```
	/// let iterator = b"bbcdabcde".iter().cloned();
	///	WaveletTreeP::new_from_iterator(iterator);
	/// ```
	pub fn new_from_iterator(vals: impl Iterator<Item = T>) -> Option<Self> {
		WaveletTreeP::new(&vals.collect::<Vec<_>>())
	}

	///Returns a WaveletTree or None
	/// # Arguments
	///
	///  ```
	/// sequence: &[T] (T must implement: Ord + Copy + Add<Output = T> + Div<Output = T> + Sub<Output = T> + PartialEq + NumCast + Debug)
	/// sequence: &[T] (T must implement: Ord + Copy + Add<Output = T> + Div<Output = T> + Sub<Output = T> + PartialEq + NumCast + Debug)
	/// ```
	//Constructor
	pub fn new(sequence: &[T]) -> Option<Self> {
		if sequence.len() == 0 {
			return None;
		}

		Some(WaveletTreeP {
			tree: WaveletTreeP::create_tree(&sequence),
		})
	}
	///Creates the BinaryTree the WaveletTree is based on
	fn create_tree(sequence: &[T]) -> BinaryTree<(RankSelect, T, T)> {
		let (left, right) = WaveletTreeP::compute_alphabet(&sequence);

		// Alphabet teilen
		let break_point = Self::compute_breakpoint(left, right);

		// bitvec mit Alphabeten erstellen
		let (bitvec, left_sequence, right_sequence) =
			WaveletTreeP::create_bitvec(&sequence, left, right, break_point);

		// (Teil-) Baum erstellen
		let mut tree: BinaryTree<(RankSelect, T, T)> =
			BinaryTree::new((RankSelect::new(bitvec, 1), left, right));

		// Rekursion

		let mut check_vec = left_sequence.clone();
		check_vec.dedup();

		if right_sequence.len() == 0 && check_vec.len() == 1 {
			return tree;
		}

		let mut check_vec_left = left_sequence.clone();
		check_vec_left.dedup();
		if check_vec_left.len() > 1 {
			tree.set_left(WaveletTreeP::create_tree(&left_sequence));
		}

		let mut check_vec_right = right_sequence.clone();
		check_vec_right.dedup();
		if check_vec_right.len() > 1 {
			tree.set_right(WaveletTreeP::create_tree(&right_sequence));
		}

		tree
	}

	///Computes the min and max of a given sequence and returns them as the interval of the alphabet.
	fn compute_alphabet(sequence: &[T]) -> (T, T) {
		// Initially computes the alphabet (left, right) from the given sequence
		let mut left: T = sequence[0];
		let mut right: T = sequence[0];
		for el in sequence {
			if el < &left {
				left = *el;
			}

			if el > &right {
				right = *el;
			}
		}
		//println!("{:?}, {:?}", left, right);
		(left, right)
	}

	/// Computes the bitvector for a given sequence.
	/// Elements that are smaller or equal than a breakpoint will be encoded as `0`.
	/// Elements that are greater than a breakpoint will be encoded as `1`.
	fn create_bitvec(
		sequence: &[T],
		left: T,
		right: T,
		break_point: T,
	) -> (BitVec, Vec<T>, Vec<T>) {
		// 0 -> left..break_point, 1 -> break_point+1..right

		// Zu erstellender Bitvektor
		let mut result_bitvec = BitVec::new_fill(false, sequence.len() as u64);

		// Resultierende Sequenzen initialisieren
		let mut result_leftvec: Vec<T> = Vec::new();
		let mut result_rightvec: Vec<T> = Vec::new();

		// Einteilung
		for i in 0..sequence.len() {
			let current_element = &sequence[i];
			// HELP
			if &current_element > &&break_point {
				result_bitvec.set(i as u64, true);
				result_rightvec.push(*current_element);
			} else {
				result_leftvec.push(*current_element);
			}
		}

		(result_bitvec, result_leftvec, result_rightvec)
	}

	/// Helper function for access.
	/// Recursively searches for the required symbol.
	fn find_symbol(tree: &BinaryTree<(RankSelect, T, T)>, i: u64) -> T {
		let (bitvec, left, right) = tree.get_item();
		if bitvec.get(i) {
			//i-th bit = true => Use Right Child
			let rank = bitvec.rank_1(i).unwrap();
			match tree.get_right() {
				Some(child) => WaveletTreeP::find_symbol(child, rank - 1),
				None => *right,
			}
		} else {
			let rank = bitvec.rank_0(i).unwrap();
			match tree.get_left() {
				Some(child) => WaveletTreeP::find_symbol(child, rank - 1),
				None => *left,
			}
		}
	}

	/// Returns the n'th symbol (index) of the encoded sequence
	pub fn access(&self, index: u64) -> T {
		WaveletTreeP::find_symbol(&self.tree, index - 1)
	}

	///Returns number of appearances of a symbol till an index
	pub fn rank(&self, index: u64, symbol: T) -> u64 {
		Self::_rank(&self.tree, index - 1, symbol)
	}

	// FIXME: _rank contains unreachable code.
	#[allow(unreachable_code)]
	fn _rank(tree: &BinaryTree<(RankSelect, T, T)>, index: u64, symbol: T) -> u64 {
		let (current_bitvec, current_left, current_right) = tree.get_item();

		if symbol <= Self::compute_breakpoint(*current_left, *current_right) {
			let zero_rank = match current_bitvec.rank_0(index) {
				Some(val) => val,
				None => return panic!("Index larger than sequence: {}", index)
			};
			if zero_rank == 0 {
				return 0;
			}
			if !tree.has_left_child() {
				return zero_rank;
			} else {
				return Self::_rank(
					tree.get_left().unwrap(),
					zero_rank - 1,
					symbol,
				);
			}
		} else {
			let one_rank = match current_bitvec.rank_1(index) {
				Some(val) => val,
				None => return panic!("Index larger than sequence: {}", index)
			};
			if one_rank == 0 {
				return 0;
			}
			if !tree.has_right_child() {
				return one_rank
			} else {
				return Self::_rank(
					tree.get_right().unwrap(),
					one_rank - 1,
					symbol,
				);
			}
		}
	}

	/// At what index is the n-th repitition of symbol.
	///
	/// Todo: Intercept errors like:
	///
	/// * symbol not in tree
	/// * symbol not as often in tree
	pub fn select(&self, mut repetition: u64, symbol: T) -> Option<u64> {
		let empty_list: Vec<&BinaryTree<(RankSelect, T, T)>> = Vec::new();
		let (rank_select, _, _): &(RankSelect, T, T) = self.tree.get_item();
		let mut node_list: Vec<&BinaryTree<(RankSelect, T, T)>> =
			WaveletTreeP::find_node(empty_list, &self.tree, symbol);
		while node_list.len() > 0 {
			let tree: &BinaryTree<(RankSelect, T, T)> = node_list.pop().unwrap();
			let (rank_select, left_alphabet, right_alphabet) = tree.get_item();
			let break_point =
				WaveletTreeP::compute_breakpoint(left_alphabet.clone(), right_alphabet.clone());
			if symbol <= break_point {
				// Position of repetition-th zero
				repetition = rank_select.select_0(repetition).unwrap() + 1;
			} else {
				// Position of repetition-th one
				repetition = rank_select.select_1(repetition).unwrap() + 1;
			}
		}
		return Some(repetition - 1);
	}

	/// Returns the path from root to leaf as a List.
	/// This function works recursive.
	fn find_node<'a>(
		mut node_list: Vec<&'a BinaryTree<(RankSelect, T, T)>>,
		tree: &'a BinaryTree<(RankSelect, T, T)>,
		symbol: T,
	) -> Vec<&'a BinaryTree<(RankSelect, T, T)>> {
		let (rank_select, left_alphabet, right_alphabet): &(RankSelect, T, T) = tree.get_item();
		let break_point: T =
			WaveletTreeP::compute_breakpoint(left_alphabet.clone(), right_alphabet.clone());
		node_list.push(tree);
		if tree.is_leaf() {
			return node_list;
		}
		if symbol <= break_point {
			if !tree.has_left_child() {
				return node_list;
			} else {
				let (left_alphabet, right_alphabet): (T, T) =
					WaveletTreeP::left_alphabet(left_alphabet.clone(), right_alphabet.clone());
				let break_point: T =
					WaveletTreeP::compute_breakpoint(left_alphabet, right_alphabet);
				return WaveletTreeP::find_node(node_list, tree.get_left().unwrap(), symbol);
			}
		} else {
			if !tree.has_right_child() {
				return node_list;
			} else {
				let (left_alphabet, right_alphabet): (T, T) =
					WaveletTreeP::right_alphabet(left_alphabet.clone(), right_alphabet.clone());
				let break_point: T =
					WaveletTreeP::compute_breakpoint(left_alphabet, right_alphabet);
				return WaveletTreeP::find_node(node_list, tree.get_right().unwrap(), symbol);
			}
		}
	}

	/// This function is really just hacked together.
	/// Fell free to refactor it or just rewrite it and implement Display.
	fn print_tree(tree: &BinaryTree<(RankSelect, T, T)>) {
		let (rank_select, left_alphabet, right_alphabet): &(RankSelect, T, T) = tree.get_item();
		let mut i = 0;
		while rank_select.bits().len() > i {
			if rank_select.get(i) {
				print!("1");
			} else {
				print!("0")
			}
			i += 1;
		}
		println!("");

		if tree.has_left_child() {
			print!("Left: ");
			WaveletTreeP::print_tree(tree.get_left().unwrap());
		}
		if tree.has_right_child() {
			print!("Right: ");
			WaveletTreeP::print_tree(tree.get_right().unwrap());
		}
	}

	/// Returns an array of the saved bytestring.
	pub fn get_string(&self) -> Vec<T> {
		let mut result: Vec<T> = Vec::new();
		let mut i = 0;
		while self.len() > i {
			result.push(self.access(i));
			i += 1;
		}
		result
	}

	/// Returns the length of the underlying string of the wavelet tree.
	pub fn len(&self) -> u64 {
		let (rank_select, left_alphabet, right_alphabet): &(RankSelect, T, T) =
			self.tree.get_item();
		rank_select.bits().len()
	}

	/// Pushs a symbol on the tree by rebuilding.
	pub fn push(&mut self, symbol: T) {
		let mut s = self.get_string();
		s.push(symbol);
		self.tree = WaveletTreeP::create_tree(s.as_slice());
	}

	/// Insert symbold at index, pushing the rest to the right.
	pub fn insert(&mut self, index: usize, symbol: T) {
		let mut s = self.get_string();
		s.insert(index, symbol);
		self.tree = WaveletTreeP::create_tree(s.as_slice());
	}

	/// Test if a symbol is in the string.
	pub fn contains(&self, symbol: T) -> bool {
		match self.select(1, symbol) {
			None => false,
			Some(position) => true,
		}
	}

	/// This functions are stupidly trivial, but they don't hurt and help adding some semantic.
	fn left_alphabet(left: T, break_point: T) -> (T, T) {
		(left, break_point)
	}

	/// This functions are stupidly trivial, but they don't hurt and help adding some semantic.
	fn right_alphabet(break_point: T, right: T) -> (T, T) {
		(break_point + cast(1).unwrap(), right)
	}

	/// Compute the breakpoint for the alphabet.
	fn compute_breakpoint(left: T, right: T) -> T {
		(left + right) / cast(2).unwrap()
	}

	/// Currently unused function.
	/// May be delete
	fn symbol_in_alphabet(left: T, right: T, symbol: T) -> bool {
		left <= symbol && symbol <= right
	}
}

pub struct WaveletTreeNP<T> {
	bitvec: RankSelect,
	alphabet: Vec<T>,
	levelsize: usize,
}

impl<T: Copy + Ord + Debug> WaveletTreeNP<T> {
	pub fn new(sequence: &[T]) -> Option<Self> {
		if sequence.len() == 0 {
			None
		} else {
			
			// init
			let alphabet = Self::compute_alphabet(&sequence);
			let mut initial_sequence_list: Vec<Vec<T>> = Vec::new();
			initial_sequence_list.push(sequence.to_owned());
			let mut initial_range_list: Vec<Range<usize>> = Vec::new();
			initial_range_list.push(Range {
				start: 0,
				end: alphabet.len(),
			});

			// calculate
			let bitvec = Self::create_tree(
				sequence.len() as u64,
				&alphabet,
				initial_sequence_list,
				initial_range_list,
			);

			// return
			Some(WaveletTreeNP {
				bitvec: RankSelect::new(bitvec, 1),
				alphabet: alphabet,
				levelsize: sequence.len(),
			})
		}
	}

	fn create_tree(
		block_size: u64,
		alphabet: &[T],
		sequence_list: Vec<Vec<T>>,
		range_list: Vec<Range<usize>>,
	) -> BitVec {
		assert_eq!(sequence_list.len(), range_list.len());

		// init
		let mut new_sequence_list: Vec<Vec<T>> = Vec::new();
		let mut new_range_list: Vec<Range<usize>> = Vec::new();

		let mut result_bitvec = BitVec::new_fill(false, block_size);
		let mut total_progress = 0;

		// calculate one level
		for (i, range) in range_list.into_iter().enumerate() {

			// set active node
			let current_sequence = &sequence_list[i];

			// calculate point to split the alphabet
			let current_breakpoint = Self::compute_breakpoint(&range);
			let current_breakelement = &(alphabet[current_breakpoint - 1]);

			// create bitvec for the active node in regards to the breakpoint
			let (left, right, mut bitvec) =
				Self::create_bitvec(current_sequence.to_owned(), current_breakelement);

			if range.end - range.start == 1 {
				// if alphabet consists only of one symbol (basically leaf)
				new_sequence_list.push(left);
				new_range_list.push(range);
			} else {
				new_sequence_list.push(left);
				new_sequence_list.push(right);

				new_range_list.push(range.start..current_breakpoint);
				new_range_list.push(current_breakpoint..range.end);
			}

			// copy bitvec (for the current node) into the result
			for j in 0..bitvec.len() {
				if bitvec.get(j) {
					result_bitvec.set(total_progress, true);
				}
				total_progress = total_progress + 1;
			}
		}


		// append and enter recursion
		for i in 0..result_bitvec.len() {
			if result_bitvec.get(i) {
				return Self::append_bitvec(
					result_bitvec,
					Self::create_tree(block_size, &alphabet, new_sequence_list, new_range_list),
				);
			}
		}

		return result_bitvec;
	}

	fn create_bitvec(sequence: Vec<T>, break_element: &T) -> (Vec<T>, Vec<T>, BitVec) {
		let mut result_bitvec = BitVec::new_fill(false, sequence.len() as u64);

		// Resultierende Sequenzen initialisieren
		let mut result_leftvec: Vec<T> = Vec::new();
		let mut result_rightvec: Vec<T> = Vec::new();

		for (i, el) in sequence.into_iter().enumerate() {
			if el > *break_element {
				result_bitvec.set_bit(i as u64, true);
				result_rightvec.push(el);
			} else {
				result_leftvec.push(el);
			}
		}

		(result_leftvec, result_rightvec, result_bitvec)
	}

	pub fn rank(&self, symbol: T, index: usize) -> usize {
		self._rank(
			Range {
				start: 0,
				end: self.levelsize as usize,
			},
			0..self.alphabet.len(),
			index,
			symbol,
		)
	}

	fn _rank(
		&self,
		bitvec_range: Range<usize>,
		alphabet_range: Range<usize>,
		index: usize,
		symbol: T,
	) -> usize {
		// leaf reached
		if alphabet_range.start + 1 == alphabet_range.end {
			if symbol != self.alphabet[alphabet_range.start] {
				return 0;
			}

			return self.subrank_0(bitvec_range.start, bitvec_range.end - 1);
		}

		// calculate break element
		let break_point = Self::compute_breakpoint(&alphabet_range);
		let break_element = self.alphabet[break_point - 1];

		if symbol <= break_element {
			// left subtree
			let new_bitvec_range = self.give_left_range(&bitvec_range);

			// enter recursion
			return self._rank(
				new_bitvec_range,
				alphabet_range.start..break_point,
				self.subrank_0(bitvec_range.start, bitvec_range.start + index),
				symbol,
			);
		} else {
			//right subtree
			let new_bitvec_range = self.give_right_range(&bitvec_range);

			// enter recursion
			return self._rank(
				new_bitvec_range,
				break_point..alphabet_range.end,
				self.subrank_1(bitvec_range.start, bitvec_range.start + index),
				symbol,
			);
		}
	}

	pub fn access(&self, index: usize) -> Option<T> {
		self._access(index, 0..self.levelsize, 0..self.alphabet.len())
	}

	fn _access(
		&self,
		index: usize,
		bitvec_range: Range<usize>,
		alphabet_range: Range<usize>,
	) -> Option<T> {
		// index out of bounds
		if index >= self.levelsize {
			return None;
		}

		// leaf reached
		if alphabet_range.start + 1 == alphabet_range.end {
			return Some(self.alphabet[alphabet_range.start]);
		}

		// compute breakpoint
		let break_point = Self::compute_breakpoint(&alphabet_range);

		if self.bitvec.get((bitvec_range.start + index) as u64) {
			// right subtree
			let new_bitvec_range = self.give_right_range(&bitvec_range);

			// enter recursion
			return self._access(
				self.subrank_1(bitvec_range.start, bitvec_range.start + index) - 1,
				new_bitvec_range,
				break_point..alphabet_range.end,
			);
		} else {
			// left subtree
			let new_bitvec_range = self.give_left_range(&bitvec_range);

			// enter recursion
			return self._access(
				self.subrank_0(bitvec_range.start, bitvec_range.start + index) - 1,
				new_bitvec_range,
				alphabet_range.start..break_point,
			);
		}
	}

	pub fn select(&self, symbol: T, repitition: usize) -> Option<u64> {
		self._select(
			0..self.levelsize,
			0..self.alphabet.len(),
			symbol,
			repitition,
		)
	}

	fn _select(
		&self,
		bitvec_range: Range<usize>,
		alphabet_range: Range<usize>,
		symbol: T,
		repitition: usize,
	) -> Option<u64> {
		if repitition == 0 {
			return None;
		}

		// leaf reached
		if alphabet_range.start + 1 == alphabet_range.end {
			if bitvec_range.end - bitvec_range.start < repitition {
				return None;
			}

			return Some(repitition as u64 - 1);
		}

		// calculate breakpoint
		let break_point = Self::compute_breakpoint(&alphabet_range);
		let break_element = self.alphabet[break_point - 1];

		if symbol <= break_element {
			// left subtree
			let new_bitvec_range = self.give_left_range(&bitvec_range);

			// calculate repition of 0 in the current node (via recursion)
			let sub_repitition = self._select(
				new_bitvec_range,
				alphabet_range.start..break_point,
				symbol,
				repitition,
			);

			if sub_repitition.is_none() {
				return None;
			}

			// subselect with calculated repition of 0
			let result = self.subselect_0(
				bitvec_range.start,
				bitvec_range.end - 1,
				sub_repitition.unwrap() as usize + 1,
			);
			return result;
		} else {
			// right subtree
			let new_bitvec_range = self.give_right_range(&bitvec_range);

			// calculate repition of 1 in the current node (via recursion)
			let sub_repitition = self._select(
				new_bitvec_range,
				break_point..alphabet_range.end,
				symbol,
				repitition,
			);

			if sub_repitition.is_none() {
				return None;
			}

			// subselect with calculated repition of 1
			let result = self.subselect_1(
				bitvec_range.start,
				bitvec_range.end - 1,
				sub_repitition.unwrap() as usize + 1,
			);
			return result;
		}
	}

	fn give_left_range(&self, range: &Range<usize>) -> Range<usize> {
		self.levelsize + range.start
			..self.levelsize + range.start + self.subrank_0(range.start, range.end - 1)
	}

	fn give_right_range(&self, range: &Range<usize>) -> Range<usize> {
		self.levelsize + range.start + self.subrank_0(range.start, range.end - 1)
			..self.levelsize + range.end
	}

	fn subrank_0(&self, from: usize, to: usize) -> usize {
		if from == 0 {
			return self.bitvec.rank_0(to as u64).unwrap() as usize;
		} else {
			return (self.bitvec.rank_0(to as u64).unwrap()
				- self.bitvec.rank_0(from as u64 - 1).unwrap()) as usize;
		}
	}

	fn subrank_1(&self, from: usize, to: usize) -> usize {
		if from == 0 {
			return self.bitvec.rank_1(to as u64).unwrap() as usize;
		} else {
			return (self.bitvec.rank_1(to as u64).unwrap()
				- self.bitvec.rank_1(from as u64 - 1).unwrap()) as usize;
		}
	}

	fn subselect_0(&self, from: usize, to: usize, repitition: usize) -> Option<u64> {
		let mut temp_bv = BitVec::new_fill(false, (to - from) as u64 + 1);

		let mut progress = 0;

		for i in from..to + 1 {
			if self.bitvec.get(i as u64) {
				temp_bv.set(progress, true);
			}
			progress = progress + 1;
		}

		let temp_rs = RankSelect::new(temp_bv, 1);

		return temp_rs.select_0(repitition as u64);
	}

	fn subselect_1(&self, from: usize, to: usize, repitition: usize) -> Option<u64> {
		let mut temp_bv = BitVec::new_fill(false, (to - from) as u64 + 1);

		let mut progress = 0;

		for i in from..to + 1 {
			if self.bitvec.get(i as u64) {
				temp_bv.set(progress, true);
			}
			progress = progress + 1;
		}

		let temp_rs = RankSelect::new(temp_bv, 1);

		return temp_rs.select_1(repitition as u64);
	}

	fn compute_alphabet(sequence: &[T]) -> Vec<T> {
		let mut alphabet = sequence.to_vec();
		alphabet.sort();
		alphabet.dedup();
		alphabet
	}

	fn compute_breakpoint(range: &Range<usize>) -> usize {
		let size = (range.end) - range.start;

		if size <= 2 {
			return range.start + 1;
		}

		let power = ((size as f64).log2() - 1.0).ceil() as u32;
		let offset = 2usize.pow(power);
		(range.start + offset)
	}

	fn append_bitvec(a: BitVec, b: BitVec) -> BitVec {
		let mut result = a.clone();
		let mut to_append = b.clone();

		for i in 0..to_append.len() {
			result.push(to_append.get(i));
		}

		result
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[quickcheck]
	fn random_access_P(sequence: Vec<u8>) -> quickcheck::TestResult {
		let tree = WaveletTreeP::new(&sequence);
		if sequence.len() == 0 {
			return quickcheck::TestResult::from_bool(tree.is_none());
		} else {
			let wtree = tree.unwrap();
			let mut rebuild_input: Vec<u8> = Vec::new();
			for (i, item) in sequence.iter().enumerate() {
				let t = wtree.access((i + 1) as u64);
				let t_ = t as u8;
				rebuild_input.push(t);
			}
			return quickcheck::TestResult::from_bool(rebuild_input == sequence);
		}
	}

	#[quickcheck]
	fn random_rank_P(sequence: Vec<u8>) -> quickcheck::TestResult {
		if sequence.len() == 0 {
			let tree = WaveletTreeP::new(&sequence);
			return quickcheck::TestResult::from_bool(tree.is_none());
		}

		let tree = WaveletTreeP::new(&sequence).unwrap();
		let mut rng = rand::thread_rng();

		let mut random_index = 1 as u64;
		if (sequence.len() > 1) {
			random_index = rng.gen_range(1, sequence.len()) as u64;
		}
		let random_element = sequence.choose(&mut rng).unwrap();

		let mut planned_result = 0;

		for i in 0..sequence.len() {
			if random_element == &sequence[i] {
				planned_result = planned_result + 1;
			}
		}

		return quickcheck::TestResult::from_bool(
			tree.rank(random_index, *random_element) == planned_result,
		);
	}

	#[test]
	fn test_empty_sequence_P() {
		let tree = WaveletTreeP::new(b"");
		assert_eq!(tree.is_none(), true);
	}

	#[test]
	fn rank_multiple_same_chars_P() {
		let tree = WaveletTreeP::new(b"aaaaa").unwrap();
		assert_eq!(tree.rank(3, b'a'), 3);
	}

	#[test]
	fn test_simple_rank_P() {
		let tree = WaveletTreeP::new(b"ab").unwrap();
		assert_eq!(tree.rank(2, b'a'), 1);
	}

	#[test]
	fn test_access_P() {
		let teststring = "abasdcdeeeh";
		let input = teststring.to_owned().into_bytes();
		let wtree = WaveletTreeP::new(&input).unwrap();
		let mut rebuild_input: String = "".to_string();
		for (i, item) in input.iter().enumerate() {
			let t = [wtree.access((i + 1) as u64)];
			let t_ = str::from_utf8(&t).unwrap();
			rebuild_input.push_str(t_);
		}
		assert_eq!(rebuild_input, teststring);
	}

	#[test]
	fn test_access_temp_P() {
		let tree = WaveletTreeP::new(b"abasdcdeeeh").unwrap();
		assert_eq!(tree.access(11), b'h');
	}

	#[test]
	fn test_select_P() {
		let test_string = b"bbcdabcde";
		let wtree: WaveletTreeP<u8> = WaveletTreeP::new(test_string).unwrap();
		let symbol: u8 = b'b';
		let repetition: u64 = 3;
		let index = wtree.select(repetition, symbol).unwrap();
		assert_eq!(index, 5);
	}

	#[test]
	fn test_new_from_iterator_P() {
		let iterator = b"bbcdabcde".iter().cloned();
		WaveletTreeP::new_from_iterator(iterator);
	}

	// NP Tests

	#[quickcheck]
	fn random_rank_NP(sequence: Vec<u8>) -> quickcheck::TestResult {
		if sequence.len() == 0 {
			let tree = WaveletTreeNP::new(&sequence);
			return quickcheck::TestResult::from_bool(tree.is_none());
		}

		let tree = WaveletTreeNP::new(&sequence).unwrap();
		let mut rng = rand::thread_rng();

		let random_element = sequence.choose(&mut rng).unwrap();
		let random_index = rng.gen_range(0, sequence.len());

		let mut planned_result = 0;

		for i in 0..sequence.len() {
			if random_element == &sequence[i] {
				planned_result = planned_result + 1;
			}
		}

		return quickcheck::TestResult::from_bool(
			tree.rank(*random_element, random_index) == planned_result,
		);
	}

	#[test]
	fn test_access_NP() {
		let teststring = "abasdcdeeeh";
		let input = teststring.to_owned().into_bytes();
		let wtree = WaveletTreeNP::new(&input).unwrap();
		let mut rebuild_input: String = "".to_string();
		for (i, item) in input.iter().enumerate() {
			let t = [wtree.access(i).unwrap()];
			let t_ = str::from_utf8(&t).unwrap();
			rebuild_input.push_str(t_);
		}
		assert_eq!(rebuild_input, teststring);
	}

	#[quickcheck]
	fn random_access_NP_filtered(sequence: Vec<u8>, index: usize) -> quickcheck::TestResult {
		if sequence.len() <= index {
			return quickcheck::TestResult::discard();
		}

		let tree = WaveletTreeNP::new(&sequence);

		if sequence.len() == 0 {
			return quickcheck::TestResult::from_bool(tree.is_none());
		} else {
			let wtree = tree.unwrap();

			return quickcheck::TestResult::from_bool(
				wtree.access(index).unwrap() == sequence[index],
			);
		}
	}

	#[quickcheck]
	fn random_access_NP_full(sequence: Vec<u8>, index: usize) -> quickcheck::TestResult {
		let tree = WaveletTreeNP::new(&sequence);

		if sequence.len() == 0 {
			return quickcheck::TestResult::from_bool(tree.is_none());
		} else {
			let wtree = tree.unwrap();

			if sequence.len() <= index {
				return quickcheck::TestResult::from_bool(wtree.access(index).is_none());
			}

			return quickcheck::TestResult::from_bool(
				wtree.access(index).unwrap() == sequence[index],
			);
		}
	}

	#[quickcheck]
	fn random_select_NP(sequence: Vec<u8>, repitition: usize) -> quickcheck::TestResult {
		let tree = WaveletTreeNP::new(&sequence);

		if sequence.len() == 0 {
			return quickcheck::TestResult::from_bool(tree.is_none());
		} else {
			let wtree = tree.unwrap();

			let mut rng = rand::thread_rng();
			let random_element = sequence.choose(&mut rng).unwrap();

			let result = None;
			let seen = 0;

			for (i, el) in sequence.clone().into_iter().enumerate() {
				if el == *random_element {
					let seen = seen + 1;
					if seen == repitition {
						let result = Some(i);
					}
				}
			}

			if result.is_none() {
				return quickcheck::TestResult::discard();
				/*

				// Keine Ahnung, warum das nicht geht

				let temp_result = wtree.select(*random_element, repitition);
				println!("checkpoint 6, temp_result: {:#?}", temp_result);
				return quickcheck::TestResult::from_bool(temp_result.is_none());
				*/
			}

			return quickcheck::TestResult::from_bool(
				wtree.select(*random_element, repitition).unwrap() == result.unwrap(),
			);
		}
	}
}
